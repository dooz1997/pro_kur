package event.handling;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Employees
{
    public StringProperty firstName;
    public StringProperty lastName;
    public StringProperty city;
    public StringProperty address;

    public Employees(String first_name, String last_name, String city, String address)
    {
	this.firstName = new SimpleStringProperty(first_name);
	this.lastName = new SimpleStringProperty(last_name);
	this.city = new SimpleStringProperty(city);
	this.address = new SimpleStringProperty(address);
    }

    public StringProperty firstNameProperty()
    {
	return firstName;
    }

    public StringProperty lastNameProperty()
    {
	return lastName;
    }

    public StringProperty cityProperty()
    {
	return city;
    }

    public StringProperty addressProperty()
    {
	return address;
    }

    // @Override
    public String toString()
    {
	return firstNameProperty().getValue();
    }
}