package event.handling;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class DataModel
{
    public static enum Source
    {
	SRC_NULL, SRC_DBASE
    };

    private ObservableList<Employees> the_personName = null;

    private Source the_src_type = Source.SRC_NULL;

    public DataModel()
    {
	the_personName = FXCollections.observableArrayList();
    }

    public DataModel(Source src_type)
    {
	this();
	this.setSourceType(src_type);
    }

    public DataModel(String source, Source src_type)
    {
	this();
	this.setData(source, src_type);
    }

    private void setSourceType(Source src_type)
    {
	this.the_src_type = src_type;
    }

    public Source getSourceType()
    {
	return the_src_type;
    }

    public ObservableList<Employees> getPersonNameData()
    {
	return the_personName;
    }

    public void setData(String source, DataModel.Source src_type)
    {
	this.setSourceType(src_type);
	this.setData(source);
    }

    public void setData(String source)
    {
		switch (getSourceType())
		{
		    case SRC_DBASE:
			this.loadDbTable(source);
			break;
		    case SRC_NULL:
			break;
		    default:
			break;
		}
    }

    public void NotWorking(String source)
    {
	loadDbTable(source);
    }

    public final void loadDbTable(String source)
    {
	final String sql = "SELECT FirstName, LastName, City, Address FROM " + source;

	try (Connection conn = this.connectDatabase();
		Statement stmt = conn.createStatement();
		ResultSet rset = stmt.executeQuery(sql))
	{

	    Employees record = null;
	    while (rset.next())
	    {
		record = new Employees(rset.getString("FirstName"), rset.getString("LastName"), rset.getString("City"),
			rset.getString("Address")
		);

		the_personName.add(record);
	    }

	} catch (SQLException e)
	{
	    System.out.println(e.getMessage());
	}

    }

    public static void addEmployeesDeatails(String firstCol, String secondCol, String thirdCol, String forthCol)
    {
	String addSQLRequestString = "INSERT INTO employees (FirstName, LastName, City, Address) Values ( \"" + firstCol
		+ "\", \"" + secondCol + "\", \"" + thirdCol + "\", \"" + forthCol + "\");";
	System.out.println(addSQLRequestString);
	try
	{
	    Statement st = DriverManager.getConnection("jdbc:sqlite:./db/chinook.db").createStatement();
	    st.execute(addSQLRequestString);
	} catch (SQLException e)
	{
	    e.printStackTrace();
	}
    }

    public static void deleteEmployeesDetails(String firstCol, String secondCol, String thirdCol, String forthCol)
    {
	String deleteSQLRequestString = "DELETE FROM employees WHERE FirstName =\"" + firstCol + "\" AND LastName = \""
		+ secondCol + "\" AND City = \"" + thirdCol + "\"AND Address = \"" + forthCol + "\";";
	System.out.println(deleteSQLRequestString);
	try
	{
	    Statement st = DriverManager.getConnection("jdbc:sqlite:./db/chinook.db").createStatement();
	    st.execute(deleteSQLRequestString);
	} catch (SQLException e)
	{
	    e.printStackTrace();
	}
    }

    public static void editEmployeesDetails(String firstCol, String secondCol, String thirdCol, String forthCol,
	    String firstColNew, String secondColNew, String thirdColNew, String forthColNew)
    {
	String editSQLRequestString = "UPDATE employees SET FirstName =\"" + firstColNew + "\", LastName = \""
		+ secondColNew + "\", City = \"" + thirdColNew + "\", Address = \"" + forthColNew
		+ "\" WHERE FirstName = \"" + firstCol + "\" AND  LastName = \"" + secondCol + "\" AND City = \""
		+ thirdCol + "\" AND Address = \"" + forthCol + "\";";
	System.out.println(editSQLRequestString);
	try
	{
	    Statement st = DriverManager.getConnection("jdbc:sqlite:./db/chinook.db").createStatement();
	    st.execute(editSQLRequestString);
	} catch (SQLException e)
	{
	    e.printStackTrace();
	}
    }

    private final Connection connectDatabase()
    {
	String url = "jdbc:sqlite:./db/chinook.db";
	Connection conn = null;
	try
	{
	    conn = DriverManager.getConnection(url);
	} catch (SQLException e)
	{
	    System.out.println(e.getMessage());
	}
	return conn;
    }

    public void close()
    {
	switch (getSourceType())
	{

	    case SRC_DBASE:
		break;
	    case SRC_NULL:
		break;
	    default:
		break;
	}

    }

}
