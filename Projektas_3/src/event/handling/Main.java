package event.handling;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class Main extends Application
{
    BorderPane root;
    Scene scene;

    DataModel the_model = null;
    FormView the_view = null;

    @Override
    public void init() throws Exception
    {
	super.init();
	this.the_model = new DataModel(DataModel.Source.SRC_DBASE);
	this.the_model.setData("employees");
    }

    @Override
    public void start(Stage stage)
    {
	try
	{
	    root = new FormView(the_model);
	    scene = new Scene(root, 650, 300);

	    scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());

	    stage.setTitle("Darbuotojų registracija");
	    stage.setScene(scene);
	    stage.show();
	} catch (Exception e)
	{
	    e.printStackTrace();
	}
    }

    public static void main(String[] args)
    {
	launch(args);
    }

    public DataModel getModel()
    {
	return this.the_model;
    }

    public FormView getView()
    {
	return this.the_view;
    }
}
