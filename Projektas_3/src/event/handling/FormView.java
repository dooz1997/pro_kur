package event.handling;

import java.util.Optional;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Separator;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class FormView extends BorderPane
{
    Label lbl_firstName;
    TextField fld_firstName;

    Label lbl_lastName;
    TextField fld_lastName;

    Label lbl_city;
    TextField fld_city;

    Label lbl_address;
    TextField fld_address;

    GridPane rec_form;

    ListView<Employees> lst_city;

    TableView<Employees> tbl_person;
    TableColumn<Employees, String> firstNameCol;
    TableColumn<Employees, String> lastNameCol;
    TableColumn<Employees, String> cityCol;
    TableColumn<Employees, String> addressCol;

    TextArea evt_info;

    MenuBar mnu_bar;
    MenuItem mit_file;
    MenuItem mit_dbase;
    MenuItem mit_exit;

    Button btn_add;
    Button btn_edit;
    Button btn_delete;
    Button btn_clear;
    Button btn_refresh;

    DataModel the_model;
    Alert alert;
    Alert error;

    public FormView(DataModel model)
    {
	initForm(model);
    }

    public void initForm(DataModel model)
    {
	this.setPadding(new Insets(10, 10, 10, 10));

	this.the_model = model;

	createMenuBar();
	createRecForm(model);
	createEvtInfo();
	createEmployeesTable(model);
	setEvtHandlers(model);
    }

    // ----------------------------------
    public void createRecForm(DataModel model)
    {
	VBox vbox = new VBox(10);
	{
	    rec_form = new GridPane();
	    {
		rec_form.setAlignment(Pos.TOP_LEFT);
		rec_form.setHgap(20);
		rec_form.setVgap(10);
		rec_form.setPadding(new Insets(25, 25, 25, 25));

		lbl_firstName = new Label("Vardas:");
		fld_firstName = new TextField();

		lbl_lastName = new Label("Pavard�:");
		fld_lastName = new TextField();

		lbl_city = new Label("Miestas:");
		fld_city = new TextField();

		lbl_address = new Label("Adresas");
		fld_address = new TextField();

		rec_form.add(lbl_firstName, 0, 0);
		rec_form.add(lbl_lastName, 0, 1);
		rec_form.add(lbl_city, 0, 2);
		rec_form.add(lbl_address, 0, 3);

		rec_form.add(fld_firstName, 1, 0, 6, 1);
		rec_form.add(fld_lastName, 1, 1, 6, 1);
		rec_form.add(fld_city, 1, 2, 6, 1);
		rec_form.add(fld_address, 1, 3, 6, 1);

		rec_form.setMinWidth(300);
	    }
	    vbox.getChildren().add(rec_form);

	    HBox btn_bar = new HBox(10);
	    {
		btn_add = new Button("Prid�ti");
		btn_edit = new Button("Redaguoti");
		btn_delete = new Button("I�trinti");
		btn_clear = new Button("I�valyti");
		btn_refresh = new Button("Atnaujinti");
		btn_bar.getChildren().addAll(btn_refresh, btn_add, btn_edit, btn_delete, btn_clear);
		btn_bar.setAlignment(Pos.TOP_CENTER);
	    }
	    vbox.getChildren().addAll(new Separator(), btn_bar);
	}

	this.setCenter(vbox);
    }

    // ---------------------

    @SuppressWarnings({ "rawtypes", "unchcked", })
    public void createEmployeesTable(DataModel model)
    {
	tbl_person = new TableView<Employees>();
	{
	    tbl_person.setItems(model.getPersonNameData());
	    firstNameCol = new TableColumn("First name");
	    firstNameCol.setCellValueFactory(new PropertyValueFactory<Employees, String>("firstName"));

	    lastNameCol = new TableColumn("Last name");
	    lastNameCol.setCellValueFactory(new PropertyValueFactory<Employees, String>("lastName"));

	    cityCol = new TableColumn("City");
	    cityCol.setCellValueFactory(new PropertyValueFactory<Employees, String>("city"));

	    addressCol = new TableColumn("Address");
	    addressCol.setCellValueFactory(new PropertyValueFactory<Employees, String>("address"));

	}

	tbl_person.getColumns().addAll(firstNameCol, lastNameCol, cityCol, addressCol);
	this.setLeft(tbl_person);
    }

    public void createMenuBar()
    {
	mnu_bar = new MenuBar();
	{

	    Menu mnu_file = new Menu("File");
	    {
		mit_file = new MenuItem("Open Text File");
		mit_dbase = new MenuItem("Open Data Base");
		mit_exit = new MenuItem("Exit");

		mnu_file.getItems().addAll(
			mit_file, mit_dbase, new SeparatorMenuItem(), mit_exit
		);
		mnu_bar.getMenus().add(mnu_file);
	    }
	}
	this.setTop(mnu_bar);
    }

    public void createEvtInfo()
    {
	evt_info = new TextArea();
	evt_info.setEditable(false);

    }

    public void printEvtInfo(String info)
    {
	evt_info.appendText(info);
    }

    public void setEvtHandlers(DataModel model)
    {

	mit_file.setOnAction((value) ->
	{
	    printEvtInfo("Menu Item Selected: Text File \n");
	    this.the_model = new DataModel(DataModel.Source.SRC_DBASE);
	    this.the_model.setData("WorldCities");
	    initForm(the_model);
	    printEvtInfo(">>> DataSource changed to the Data Base");
	});

	mit_file.setOnAction((value) ->
	{
	    printEvtInfo("Menu Item Selected: Data Base \n");
	    this.the_model = new DataModel(DataModel.Source.SRC_DBASE);
	    this.the_model.setData("customers");
	    initForm(the_model);
	    printEvtInfo(">>> DataSource changed to the Data Base");
	});

	mit_exit.setOnAction((value) ->
	{
	    printEvtInfo("Menu Item Selected: Exit \n");
	    Platform.exit();
	});

	tbl_person.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) ->
	{
	    showPersonDetails(newValue);
	    tbl_person.setItems(model.getPersonNameData());
	    btn_edit.setOnAction(e ->
	    {
		printEvtInfo("Button pressed: Edit \n");
		DataModel.editEmployeesDetails(newValue.firstNameProperty().get(), newValue.lastNameProperty().get(),
			newValue.cityProperty().get(), newValue.addressProperty().get(), fld_firstName.getText(),
			fld_lastName.getText(), fld_city.getText(), fld_address.getText());
	    });
	});

	fld_firstName.textProperty().addListener((observable, oldValue, newValue) ->
	{
	    printEvtInfo("City - TextField Text Changed (new value: " + newValue + ")\n");
	});

	fld_firstName.setOnAction((event) ->
	{
	    printEvtInfo("City - TextField Action.\n");
	});

	btn_refresh.setOnAction((value) ->
	{
	    printEvtInfo("Button pressed: Refresh \n");
	    RefreshMethod();
	});

	btn_delete.setOnAction((value) ->
	{
	    printEvtInfo("Button pressed: Clear \n");
	    alert = new Alert(AlertType.CONFIRMATION);
	    alert.setTitle("Confiramation Dialog");
	    alert.setHeaderText("");
	    alert.setContentText("Ar tikrai norite i�trinti?");

	    String firstCol = "", secondCol = "", thirdCol = "", forthCol = "";
	    firstCol = this.fld_firstName.getText();
	    secondCol = this.fld_lastName.getText();
	    thirdCol = this.fld_city.getText();
	    forthCol = this.fld_address.getText();

	    Optional<ButtonType> result = alert.showAndWait();
	    if (result.get() == ButtonType.OK)
	    {
		DataModel.deleteEmployeesDetails(firstCol, secondCol, thirdCol, forthCol);
		printEvtInfo("Button pressed: Delete confirmation true \n");
		ClearMethod();
	    } else
	    {
		printEvtInfo("Button pressed: Delete confirmation false \n");
		ClearMethod();
	    }
	});

	btn_clear.setOnAction((value) ->
	{
	    printEvtInfo("Button pressed: Clear \n");
	    ClearMethod();
	});

	btn_add.setOnAction((value) ->
	{
	    String firstCol = "", secondCol = "", thirdCol = "", forthCol = "";
	    if (this.fld_firstName.getText().equals(null) && this.fld_lastName.getText().equals(null)
		    && this.fld_city.getText().equals(null) && this.fld_address.getText().equals(null))
	    {
		error = new Alert(AlertType.ERROR);
		alert.setTitle("Error Dialog");
		alert.setHeaderText("Look, an Error Dialog");
		alert.setContentText("Fill all text areas!");
		alert.showAndWait();
	    } else
	    {
		firstCol = this.fld_firstName.getText();
		secondCol = this.fld_lastName.getText();
		thirdCol = this.fld_city.getText();
		forthCol = this.fld_address.getText();
		DataModel.addEmployeesDeatails(firstCol, secondCol, thirdCol, forthCol);
	    }

	    printEvtInfo("Button pressed: Add \n");
	    ClearMethod();

	});

    }

    public void NewValues(Employees employees)
    {

    }

    public void showPersonDetails(Employees employees)
    {
	if (employees != null)
	{
	    this.fld_firstName.setText(employees.firstNameProperty().get());
	    this.fld_lastName.setText(employees.lastNameProperty().get());
	    this.fld_city.setText(employees.cityProperty().get());
	    this.fld_address.setText(employees.addressProperty().get());
	} else
	{
	    ClearMethod();
	}
    }

    public void ClearMethod()
    {
	this.fld_firstName.setText(null);
	this.fld_lastName.setText(null);
	this.fld_city.setText(null);
	this.fld_address.setText(null);
    }

    public void RefreshMethod()
    {

	this.the_model = new DataModel(DataModel.Source.SRC_DBASE);
	this.the_model.setData("employees");
	initForm(the_model);
    }
}
